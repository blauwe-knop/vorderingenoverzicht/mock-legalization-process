// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"go.uber.org/zap"

	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/repositories"
	sessionRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/internal/usecases"
)

type options struct {
	ListenAddress                         string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the mock-legalization-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	LegalizationServiceAddress            string `long:"legalization-service-address" env:"LEGALIZATION_SERVICE_ADDRESS" default:"http://localhost:80" description:"Legalization service address."`
	LegalizationServiceAPIKey             string `long:"legalization-service-api-key" env:"LEGALIZATION_SERVICE_API_KEY" default:"" description:"API key to use when calling the Legalization service."`
	SessionServiceAddress                 string `long:"session-service-address" env:"SESSION_SERVICE_ADDRESS" default:"http://localhost:80" description:"Session service address."`
	SessionServiceAPIKey                  string `long:"session-service-api-key" env:"SESSION_SERVICE_API_KEY" default:"" description:"API key to use when calling the session service."`
	BkConfigServiceAddress                string `long:"bk-config-service-address" env:"BK_CONFIG_SERVICE_ADDRESS" default:"http://localhost:80" description:"Bk Config service address."`
	BkConfigServiceAPIKey                 string `long:"bk-config-service-api-key" env:"BK_CONFIG_SERVICE_API_KEY" default:"" description:"API key to use when calling the bk-config service."`
	LegalizationExpirationMinutes         string `long:"legalization-expiration-minutes" env:"LEGALIZATION_EXPIRATION_MINUTES" default:"5" description:"Max length of a legalization in minutes."`
	LegalizedCertificateExpirationMinutes string `long:"legalized-certificate-expiration-minutes" env:"LEGALIZED_CERTIFICATE_EXPIRATION_MINUTES" default:"525960" description:"Max length of a legalized certificate in minutes."`

	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	legalizationRepository := repositories.NewLegalizationClient(cliOptions.LegalizationServiceAddress, cliOptions.LegalizationServiceAPIKey)
	sessionRepository := sessionRepositories.NewSessionClient(cliOptions.SessionServiceAddress, cliOptions.SessionServiceAPIKey)
	bkConfigRepository := bkConfigRepositories.NewBkConfigClient(cliOptions.BkConfigServiceAddress, cliOptions.BkConfigServiceAPIKey)

	legalizationExpirationMinutes, err := strconv.Atoi(cliOptions.LegalizationExpirationMinutes)
	if err != nil {
		log.Fatalf("failed parse legalizationExpirationMinutes to int: %v", err)
	}
	legalizedCertificateExpirationMinutes, err := strconv.Atoi(cliOptions.LegalizedCertificateExpirationMinutes)
	if err != nil {
		log.Fatalf("failed parse legalizedCertificateExpirationMinutes to int: %v", err)
	}

	legalizationUseCase := usecases.NewLegalizationUseCase(
		logger,
		legalizationRepository,
		sessionRepository,
		bkConfigRepository,
		legalizationExpirationMinutes,
		legalizedCertificateExpirationMinutes,
	)

	router := http_infra.NewRouter(legalizationUseCase)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
