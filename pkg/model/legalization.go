// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type Legalization struct {
	Token string `json:"token"`
}
