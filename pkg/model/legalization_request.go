// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type LegalizationRequest struct {
	AppPublicKey string `json:"appPublicKey"`
}
