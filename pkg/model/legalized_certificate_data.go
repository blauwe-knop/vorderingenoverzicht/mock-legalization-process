// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type LegalizedCertificateData struct {
	AppPublicKey         string   `json:"appPublicKey"`
	Bsn                  string   `json:"bsn"`
	LegalizatorOin       string   `json:"legalizatorOin"`
	LegalizatorPublicKey string   `json:"legalizatorPublicKey"`
	IssuedAt             JSONTime `json:"issuedAt"`
	NotBefore            JSONTime `json:"notBefore"`
	NotAfter             JSONTime `json:"notAfter"`
}
