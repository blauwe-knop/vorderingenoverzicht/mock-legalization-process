// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type LegalizationResult struct {
	EncryptedCertificate string `json:"encryptedCertificate"`
}
