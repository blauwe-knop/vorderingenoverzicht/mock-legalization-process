// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type Certificate interface {
	GetCertificateType() string
}

type BaseCertificate struct {
	CertificateType string `json:"certificateType"`
}

func (c BaseCertificate) GetCertificateType() string {
	return c.CertificateType
}

func (c LegalizedCertificate) GetCertificateType() string {
	return c.CertificateType
}

type LegalizedCertificate struct {
	AppPublicKey         string   `json:"appPublicKey"`
	Bsn                  string   `json:"bsn"`
	LegalizatorOin       string   `json:"legalizatorOin"`
	LegalizatorPublicKey string   `json:"legalizatorPublicKey"`
	IssuedAt             JSONTime `json:"issuedAt"`
	NotBefore            JSONTime `json:"notBefore"`
	NotAfter             JSONTime `json:"notAfter"`
	CertificateSignature string   `json:"certificateSignature"`
	BaseCertificate
}
