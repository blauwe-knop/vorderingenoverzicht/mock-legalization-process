// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type UserIdentity struct {
	Bsn string `json:"bsn"`
}
