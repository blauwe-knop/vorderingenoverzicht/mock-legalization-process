module gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.10
	github.com/jessevdk/go-flags v1.5.0
	gitlab.com/blauwe-knop/common/health-checker v0.0.3
	go.uber.org/zap v1.25.0
)

require gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service v0.12.1

require (
	github.com/go-chi/cors v1.2.1
	github.com/google/uuid v1.3.0
	gitlab.com/blauwe-knop/common/rsa-mobile v1.3.5
	gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service v0.12.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/session-service v0.12.1
)

require (
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.2.0 // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/lestrrat-go/backoff/v2 v2.0.8 // indirect
	github.com/lestrrat-go/blackmagic v1.0.1 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/jwx v1.2.26 // indirect
	github.com/lestrrat-go/option v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.12.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	software.sslmate.com/src/go-pkcs12 v0.2.1 // indirect
)
