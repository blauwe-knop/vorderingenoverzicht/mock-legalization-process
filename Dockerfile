FROM golang:1-alpine AS build

RUN apk add --update --no-cache git

ADD . /go/src/mock-legalization-process/
WORKDIR /go/src/mock-legalization-process
RUN go mod download
RUN go build -o dist/bin/mock-legalization-process ./cmd/mock-legalization-process

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/mock-legalization-process/dist/bin/mock-legalization-process /usr/local/bin/mock-legalization-process
COPY --from=build /go/src/mock-legalization-process/api/openapi.json /api/openapi.json
COPY --from=build /go/src/mock-legalization-process/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/mock-legalization-process"]
