// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitlab.com/blauwe-knop/common/rsa-mobile/rsa"

	"github.com/google/uuid"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	bkConfigModel "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/model"
	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/repositories"
	sessionRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/pkg/model"
)

type LegalizationUseCase struct {
	Logger                                *zap.Logger
	LegalizationRepository                serviceRepositories.LegalizationRepository
	SessionRepository                     sessionRepositories.SessionRepository
	BkConfigRepository                    bkConfigRepositories.BkConfigRepository
	LegalizationExpirationMinutes         int
	LegalizedCertificateExpirationMinutes int
}

var ErrKeyPairDoesNotExist = errors.New("error key pair does not exist")
var ErrSessionExpired = errors.New("error session expired")
var ErrLegalizationExpired = errors.New("error legalization expired")

func NewLegalizationUseCase(logger *zap.Logger, legalizationRepository serviceRepositories.LegalizationRepository, sessionRepository sessionRepositories.SessionRepository, bkConfigRepository bkConfigRepositories.BkConfigRepository, legalizationExpirationMinutes int, legalizedCertificateExpirationMinutes int) *LegalizationUseCase {
	return &LegalizationUseCase{
		Logger:                                logger,
		LegalizationRepository:                legalizationRepository,
		SessionRepository:                     sessionRepository,
		BkConfigRepository:                    bkConfigRepository,
		LegalizationExpirationMinutes:         legalizationExpirationMinutes,
		LegalizedCertificateExpirationMinutes: legalizedCertificateExpirationMinutes,
	}
}

func (uc *LegalizationUseCase) StartLegalization(sessionToken string, legalizationRequest model.LegalizationRequest) (*model.Legalization, error) {
	session, err := uc.SessionRepository.Get(sessionToken)
	if err != nil {
		return nil, fmt.Errorf("failed to get session: %v", err)
	}

	if time.Now().After((time.Time)(session.ExpiresAt)) {
		return nil, ErrSessionExpired
	}

	if session.AppPublicKey != legalizationRequest.AppPublicKey {
		return nil, fmt.Errorf("invalid session token: %v", err)
	}

	fastRsa := rsa.NewFastRSA()

	legalizationRequestPublicKeyInfo, err := fastRsa.MetadataPublicKey(legalizationRequest.AppPublicKey)
	if err != nil {
		return nil, fmt.Errorf("failed to get legalization request public key metadata: %v", err)
	}

	if legalizationRequestPublicKeyInfo.Size < 256 {
		return nil, fmt.Errorf("legalization request public key size smaller then 256: %v", legalizationRequestPublicKeyInfo.Size)
	}

	expiresAt := time.Now().Add(time.Minute * time.Duration(uc.LegalizationExpirationMinutes))

	legalization := serviceModel.Legalization{
		Token:        uuid.NewString(),
		AppPublicKey: legalizationRequest.AppPublicKey,
		CreatedAt:    serviceModel.JSONTime(time.Now()),
		ExpiresAt:    serviceModel.JSONTime(expiresAt),
	}

	createdLegalization, err := uc.LegalizationRepository.Create(legalization)
	if err != nil {
		return nil, fmt.Errorf("failed to create legalization: %v", err)
	}

	return &model.Legalization{Token: createdLegalization.Token}, nil
}

func (uc *LegalizationUseCase) CompleteLegalization(legelizationToken string, userIdentity model.UserIdentity) (*model.Legalization, error) {
	legalization, err := uc.LegalizationRepository.Get(legelizationToken)
	if err != nil {
		return nil, fmt.Errorf("failed to get legalization: %v", err)
	}

	if time.Now().After((time.Time)(legalization.ExpiresAt)) {
		return nil, ErrLegalizationExpired
	}

	legalization.Bsn = userIdentity.Bsn

	updatedLegalization, err := uc.LegalizationRepository.Update(legalization.Token, *legalization)
	if err != nil {
		return nil, fmt.Errorf("failed to update legalization: %v", err)
	}

	return &model.Legalization{
		Token: updatedLegalization.Token,
	}, nil
}

func (uc *LegalizationUseCase) FetchLegalization(sessionToken string, legalizationToken string) (*model.LegalizationResult, error) {
	session, err := uc.SessionRepository.Get(sessionToken)
	if err != nil {
		return nil, fmt.Errorf("failed to get session: %v", err)
	}

	if time.Now().After((time.Time)(session.ExpiresAt)) {
		return nil, ErrSessionExpired
	}

	legalization, err := uc.LegalizationRepository.Get(legalizationToken)
	if err != nil {
		return nil, fmt.Errorf("failed to get legalization: %v", err)
	}

	if time.Now().After((time.Time)(legalization.ExpiresAt)) {
		return nil, ErrLegalizationExpired
	}

	if session.AppPublicKey != legalization.AppPublicKey {
		return nil, fmt.Errorf("invalid session token: %v", err)
	}

	fastRsa := rsa.NewFastRSA()

	legalizationAppPublicKeyInfo, err := fastRsa.MetadataPublicKey(legalization.AppPublicKey)
	if err != nil {
		return nil, fmt.Errorf("failed to get legalization app public key metadata: %v", err)
	}

	if legalizationAppPublicKeyInfo.Size < 256 {
		return nil, fmt.Errorf("legalization app public key size smaller then 256: %v", legalizationAppPublicKeyInfo.Size)
	}

	keyPair, err := uc.getKeyPair()
	if err != nil {
		return nil, fmt.Errorf("failed retrieve key pair: %v", err)
	}

	keyPairPublicKeyInfo, err := fastRsa.MetadataPublicKey(keyPair.PublicKey)
	if err != nil {
		return nil, fmt.Errorf("failed to get key pair public key metadata: %v", err)
	}

	if keyPairPublicKeyInfo.Size < 256 {
		return nil, fmt.Errorf("key pair public key size smaller then 256: %v", keyPairPublicKeyInfo.Size)
	}

	legalizedCertificateData := &model.LegalizedCertificateData{
		AppPublicKey:         legalization.AppPublicKey,
		Bsn:                  legalization.Bsn,
		LegalizatorOin:       "00000001001172773000",
		LegalizatorPublicKey: keyPair.PublicKey,
		IssuedAt:             model.JSONTime(time.Now()),
		NotBefore:            model.JSONTime(time.Now()),
		NotAfter:             model.JSONTime(time.Now().Add(time.Minute * time.Duration(uc.LegalizedCertificateExpirationMinutes))),
	}

	legalizedCertificateDataAsJson, err := json.Marshal(legalizedCertificateData)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal legalized certificate data: %v", err)
	}

	privateKeyInfo, err := fastRsa.MetadataPrivateKey(keyPair.PrivateKey)
	if err != nil {
		return nil, fmt.Errorf("failed to get private key metadata: %v", err)
	}

	if privateKeyInfo.Size < 256 {
		return nil, fmt.Errorf("private key size smaller then 256: %v", privateKeyInfo.Size)
	}

	certificateSignature, err := fastRsa.SignPSS(
		string(legalizedCertificateDataAsJson),
		"sha512",
		"equalsHash",
		keyPair.PrivateKey)
	if err != nil {
		return nil, fmt.Errorf("failed to sign message: %v", err)
	}

	legalizedCertificate := &model.LegalizedCertificate{
		AppPublicKey:         legalizedCertificateData.AppPublicKey,
		Bsn:                  legalizedCertificateData.Bsn,
		LegalizatorOin:       legalizedCertificateData.LegalizatorOin,
		LegalizatorPublicKey: legalizedCertificateData.LegalizatorPublicKey,
		IssuedAt:             legalizedCertificateData.IssuedAt,
		NotBefore:            legalizedCertificateData.NotBefore,
		NotAfter:             legalizedCertificateData.NotAfter,
		CertificateSignature: certificateSignature,
		BaseCertificate: model.BaseCertificate{
			CertificateType: "LegalizedCertificate",
		},
	}

	legalizedCertificateAsJson, err := json.Marshal(legalizedCertificate)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal legalized certificate: %v", err)
	}

	encryptedCertificate, err := fastRsa.EncryptOAEP(string(legalizedCertificateAsJson), "legalized-certificate", "sha512", legalization.AppPublicKey)
	if err != nil {
		return nil, fmt.Errorf("failed to encrypt legalized certificate: %v", err)
	}

	return &model.LegalizationResult{
		EncryptedCertificate: encryptedCertificate,
	}, nil
}

func (uc *LegalizationUseCase) getKeyPair() (*bkConfigModel.KeyPair, error) {
	// current implemetation: we always pick the first one if multiple keys exist
	keyPairList, err := uc.BkConfigRepository.ListKeyPairs()
	if err != nil {
		return nil, fmt.Errorf("failed to list key pairs: %v", err)
	}

	if len(*keyPairList) == 0 {
		return nil, ErrKeyPairDoesNotExist
	}

	keyPair := (*keyPairList)[0]

	return &keyPair, nil
}

func (uc *LegalizationUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		uc.LegalizationRepository,
	}
}
