// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/go-chi/cors"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/internal/usecases"
)

type key int

const (
	legalizationUseCaseKey key = iota
)

func NewRouter(legalizationUseCase *usecases.LegalizationUseCase) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	})

	r.Use(cors.Handler)

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/start_legalization", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationUseCaseKey, legalizationUseCase)
				handlerStartLegalization(w, r.WithContext(ctx))
			})
		})

		r.Route("/complete_legalization", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationUseCaseKey, legalizationUseCase)
				handlerCompleteLegalization(w, r.WithContext(ctx))
			})
		})

		r.Route("/fetch_legalization", func(r chi.Router) {
			r.Use(middleware.Logger)
			r.Post("/", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationUseCaseKey, legalizationUseCase)
				handlerFetchLegalization(w, r.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(w http.ResponseWriter, r *http.Request) {
			handlerJson(w, r)
		})

		r.Get("/openapi.yaml", func(w http.ResponseWriter, r *http.Request) {
			handlerYaml(w, r)
		})

		healthCheckHandler := healthcheck.NewHandler("mock-legalization-process", legalizationUseCase.GetHealthChecks())
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}
