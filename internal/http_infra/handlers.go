// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/pkg/model"
)

func handlerStartLegalization(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationUseCase, _ := ctx.Value(legalizationUseCaseKey).(*usecases.LegalizationUseCase)

	providedSessionToken := r.Header.Get("Authorization")

	var legalizationRequest model.LegalizationRequest
	err := json.NewDecoder(r.Body).Decode(&legalizationRequest)
	if err != nil {
		log.Printf("failed to decode request payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	legalization, err := legalizationUseCase.StartLegalization(providedSessionToken, legalizationRequest)

	if err != nil {
		log.Printf("error StartLegalization: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(legalization)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerCompleteLegalization(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationUseCase, _ := ctx.Value(legalizationUseCaseKey).(*usecases.LegalizationUseCase)

	providedToken := r.Header.Get("Authorization")

	var legalizationUpdate model.UserIdentity
	err := json.NewDecoder(r.Body).Decode(&legalizationUpdate)
	if err != nil {
		log.Printf("failed to decode request payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	legalization, err := legalizationUseCase.CompleteLegalization(providedToken, legalizationUpdate)

	if err != nil {
		log.Printf("error CompleteLegalization: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(legalization)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerFetchLegalization(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationUseCase, _ := ctx.Value(legalizationUseCaseKey).(*usecases.LegalizationUseCase)

	sessionToken := r.Header.Get("Authorization")
	legalizationToken := r.URL.Query().Get("legalizationToken")

	legalizationResult, err := legalizationUseCase.FetchLegalization(sessionToken, legalizationToken)

	if err != nil {
		log.Printf("error FetchLegalization: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(legalizationResult)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
