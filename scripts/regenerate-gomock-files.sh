#!/bin/bash

mockgen -destination=./../test/mock/legalization_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/repositories LegalizationRepository
mockgen -destination=./../test/mock/session_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories SessionRepository
