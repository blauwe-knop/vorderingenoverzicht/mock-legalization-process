# mock-legalization-process

Procescomponent of participating organizations in the 'Vorderingenoverzicht' system.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Now start the session process:

```sh
go run cmd/mock-legalization-process/main.go
```

By default, the session process will run on port `80`.

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

### Regenerating mocks

```sh
sh scripts/regenerate-gomock-files.sh
```

## Deployment
